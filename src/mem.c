#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/mman.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

#define BLOCK_MIN_CAPACITY 24

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    struct region R = {0};
    query = region_actual_size(query);
    block_size block_sz = {query};
    R.addr = map_pages(addr, query, 0);
    if (region_is_invalid(&R)){
      struct region t = {0};
      return t;
    }
        
    R.size = query;
    
    block_init(R.addr, block_sz, R.addr + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY );
    return R;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial_size ) {
  const struct region region = alloc_region( HEAP_START, initial_size );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}



/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (block_splittable(block, query))
    {
        void* temp = block->next;
        block->next = (void*)(block->contents + query);
        block->next->capacity.bytes = block->capacity.bytes - query;
        block->capacity.bytes = (BLOCK_MIN_CAPACITY <= query)?query:BLOCK_MIN_CAPACITY;
        block->next->next = temp;
        return 1;
    }
    return 0;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )    {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
  //int a = malloc(4);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  bool b1 = fst->is_free;
  bool b2 = snd->is_free;
  bool b3 = blocks_continuous( fst, snd );
  return b1 && b2 && b3;
}

static bool try_merge_with_next( struct block_header* block ) {
    
    void* after = block_after(block);
    if (mergeable(block, after) )
    {
      block->next = ((struct block_header *)after)->next;
      block->capacity.bytes += ((struct block_header *)after)->capacity.bytes + offsetof(struct block_header, contents);
      return 1;
    }
    return 0;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {

    sz = (BLOCK_MIN_CAPACITY <= sz) ? sz : BLOCK_MIN_CAPACITY;
    while (true) {
        if (block->is_free) {
                //printf("\nfind_good_or_last\n");
            //while (try_merge_with_next(block));
                //printf("\nfind_good_or_last\n");
            if (block_is_big_enough(sz, block)){
                //printf("\nfind_good_or_last\n");
                split_if_too_big(block, sz);
                //printf("\nfind_good_or_last\n");
                struct block_search_result Res;
                //printf("\nfind_good_or_last\n");
                Res.type = BSR_FOUND_GOOD_BLOCK;
                //printf("\nfind_good_or_last\n");
                Res.block = block;
                //printf("\nfind_good_or_last\n");
                return Res;
            }
        }
        else {
            if (block->next == NULL) return (struct block_search_result) { .type = BSR_REACHED_END_NOT_FOUND, .block = block };
            else {
                block = block->next;
            }
        }
    }
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    return find_good_or_last(block, query);
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
 
    struct region Reg; 
    Reg = alloc_region(block_after(last), query);
    if (region_is_invalid(&Reg)) return NULL;
    last->next = Reg.addr;
    if (try_merge_with_next(last)) return last;
    return last->next;

}

static int DoINeedNewRegion = 1;
/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

    //struct block_header * header = (struct block_header *) &Reg;
    //struct block_header* header = (struct block_header*)heap_start;
    struct block_search_result RR;
    RR = try_memalloc_existing(query, heap_start);
    //printf("\nmemalloc\n");
    struct block_header* temp = RR.block;
    if (RR.type == BSR_REACHED_END_NOT_FOUND) {
      //printf("\n\n-----\n\n");
        temp = grow_heap(temp, query);
        split_if_too_big(temp, query);
        RR.block = temp;
    }

    RR.block->is_free = false;
    return RR.block;

}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
}
