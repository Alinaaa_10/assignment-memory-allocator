#include <stdio.h>
#include "mem.h"
int main()
{
    void* heap = heap_init(10000);
    {char str[] = "------------------ Test_1 ---------------\n";
    printf("%s", str);
    void* chunk_1 = _malloc(70);
    void* chunk_2 = _malloc(500);
    debug_heap(stdout, heap);
    _free(chunk_1);
    _free(chunk_2);
    printf("----------------End Of Test_1 -------------------------\n");}

    {
    printf("------------------ Test_2 ---------------\n");
    void* chunk_1 = _malloc(70);
    void* chunk_2 = _malloc(500);
    _free(chunk_1);
    debug_heap(stdout, heap);
    _free(chunk_2);
    printf("----------------End Of Test_2 -------------------------\n");}


    {printf("------------------ Test_3 ---------------\n");
    void* chunk_1 = _malloc(70);
    void* chunk_2 = _malloc(500);
    void* chunk_3 = _malloc(1500);
    _free(chunk_1);
    _free(chunk_2);
    debug_heap(stdout, heap);
    _free(chunk_3);
    printf("----------------End Of Test_3 -------------------------\n");}


    {printf("------------------ Test_4 ---------------\n");
    void* chunk_1 = _malloc(8000);
    void* chunk_2 = _malloc(4000);
    debug_heap(stdout, heap);
    _free(chunk_1);
    _free(chunk_2);
    printf("----------------End Of Test_4 -------------------------\n");}


    {printf("------------------ Test_5 ---------------\n");
    void* chunk_1 = _malloc(14000);
    void* chunk_2 = _malloc(50000);
    debug_heap(stdout, heap);
    _free(chunk_1);
    _free(chunk_2);
    printf("----------------End Of Test_5 -------------------------\n");}


    return 0;
}